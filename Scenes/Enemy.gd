extends Area2D

var screensize
export var speed = 100
export var forward_step = 50

var direction

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size
	direction = "left"

func _process(delta):
	var velocity = 0
	if direction == "left":
		velocity = -1
	else:
		velocity = 1

	position.x += velocity * speed * delta
	
	if position.x < -50 or position.x > screensize.x + 50:
		position.y += forward_step
		if direction == "left":
			direction = "right"
		else:
			direction = "left"