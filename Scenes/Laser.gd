extends Area2D

export var speed = 500

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	position.y -= speed * delta
	if position.y < -50:
		queue_free()

func _on_Laser_area_entered(area):
	if area.is_in_group("enemy"):
		print("Halo kawan semua, ayo kita sambut!")
		print(area)
		area.queue_free()
		queue_free()
