extends Area2D

var screensize
export var maxspeed = 400
export var acceleration_rate = 40
onready var bullet = preload("res://Scenes/Laser.tscn")

var velocity = 0

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size
	pass

func _process(delta):
	
	if Input.is_action_pressed("ui_right"):
		velocity += acceleration_rate
#		velocity = 300
	elif Input.is_action_pressed("ui_left"):
		velocity -= acceleration_rate
#		velocity = -300
	else:
		var neg = -1 if velocity < 0 else 1
		velocity = max(0, abs(velocity) - (acceleration_rate) * 1.5) * neg
	velocity = clamp(velocity, -maxspeed, maxspeed)

	position.x += velocity * delta
	if position.x < 0 or position.x > screensize.x:
		position.x = clamp(position.x, 0, screensize.x)
		velocity = 0
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)

func _on_Laser_area_entered(area):
	pass # replace with function body
